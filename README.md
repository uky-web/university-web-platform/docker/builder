# Builder

A [publicly available](https://hub.docker.com/r/newcity/builder/) Docker image for running CI builds/deployments.  To use in Gitlab (Docker) CI,

```
image: newcity/builder:[version]
```

at the top of your repo's .gitlab-ci.yml file, where `[version]` is the most recent tag you can find.

Currently available

- node / npm
- rsync
- php5-cli
- php5-curl
- unzip
- git
- openssh-client
- less

- gulp
- yarn
- composer

- terminus

## Continuous integration

A push to the master branch will build this Docker image with tag `newcity/builder:latest` and push to the [Docker Hub](https://hub.docker.com/r/newcity/builder/tags/). Your `newcity/builder:latest` image won't be updated automatically, however.  You'll need to force your local docker or the gitlab runner to re-pull the image, and it's probably easiest to just clear the image cache (see below).

## Tagging this image explicitly

With a `[tag]` in mind:

```bash
docker build --no-cache -t newcity/builder:[tag] .
docker push newcity/builder
```

## Clearing this image from the local Docker cache

```bash
docker rmi $( docker images -a | grep newcity/builder | awk '{print $3}')
```
