FROM node:8
MAINTAINER NewCity <geeks@insidenewcity.com>

# Let the container know that there is no tty
ENV DEBIAN_FRONTEND noninteractive
ENV COMPOSER_NO_INTERACTION 1
ENV PHP_VERSION 7.2

# Tell npm to display only warnings and errors
ENV NPM_CONFIG_LOGLEVEL warn

# set working dir
ENV HOME /root
WORKDIR /root

# Add basic repository
RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y \
    apt-utils \
    git \
    zip \
    unzip \
    libfontconfig \
    openssh-client \
    rsync \
    less \
    apt-transport-https \
    lsb-release \
    ca-certificates \
    jq \
    moreutils

RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg \
 && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list \
 && apt-get update \
 && apt-get install -y \
    php${PHP_VERSION}-cli \
    php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-curl \
    php${PHP_VERSION}-json \
    php${PHP_VERSION}-xml \
    php${PHP_VERSION}-zip \
    php${PHP_VERSION}-bz2 \
    php${PHP_VERSION}-gd

RUN rm -rf /var/lib/apt/lists/*

# Install Node tools
RUN npm install -g npm@latest

RUN npm install -g \
    gulp@3 \
    pl2html \
    install_module_dependencies \
    manual_core_patch

# Install yarn and set its cache for Docker
RUN curl -o- -L https://yarnpkg.com/install.sh | bash
RUN mkdir -p /cache/yarn && /root/.yarn/bin/yarn config set cache-folder /cache/yarn
ENV PATH "$PATH:/root/.yarn/bin"

# Install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
  php composer-setup.php && \
  rm composer-setup.php && \
  mv /root/composer.phar /usr/bin/composer && \
  chmod +x /usr/bin/composer

# Install terminus
RUN curl -O https://raw.githubusercontent.com/pantheon-systems/terminus-installer/master/builds/installer.phar && \
    php installer.phar install --install-dir=/usr/local && \
  chmod +x /usr/local/bin/terminus

# Terminus plugins
RUN mkdir -p /root/terminus/plugins
RUN composer create-project -d ~/terminus/plugins pantheon-systems/terminus-composer-plugin:~1

# Faster composer
RUN composer global require hirak/prestissimo